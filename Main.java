package com.company;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	double[] a = new double[]{2,28,13,37,14,88,3,22};

	for (int i = 0;i < a.length;i++){
	    a[i] *= 1.1;
	}
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] < a[j + 1]) {
                    double tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                }
            }
        }
        String aStr = Arrays.toString(a);
        System.out.println(aStr);
    }
}
